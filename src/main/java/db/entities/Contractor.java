package db.entities;

public class Contractor {
    public String inn;
    public String phone;

    public Contractor(String inn, String phone) {
        this.inn = inn;
        this.phone = phone;
    }
}
