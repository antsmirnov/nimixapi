package api;

import api.service.AuthRefreshToken;
import api.service.GetInfoDepositBalance;
import api.service.AuthLogin;
import api.service.TaxCheckContractorStatus;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.net.ssl.*;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.cert.CertificateException;

public class Api {
    public static String savedAccessToken;
    public static String savedRefreshToken;

    // Redefined OkHttpClient for debug purposes (local proxy added and non-safe connections allowed)
    public static OkHttpClient getUnsafeOkHttpClientWithProxy() {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 5555))).build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://nm-test.mmtr.ru/nmapi/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(getUnsafeOkHttpClientWithProxy())                   // For debug purposes
            .build();


    public static AuthLogin login = retrofit.create(AuthLogin.class);
    public static AuthRefreshToken refreshToken = retrofit.create(AuthRefreshToken.class);
    public static GetInfoDepositBalance getInfoDepositBalance = retrofit.create(GetInfoDepositBalance.class);
    public static TaxCheckContractorStatus checkContractorStatus = retrofit.create(TaxCheckContractorStatus.class);


}
