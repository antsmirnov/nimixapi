package api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxCheckContractorStatusModel {
    @SerializedName("errorCode")
    @Expose
    public String errorCode;
    @SerializedName("errorMessage")
    @Expose
    public String errorMessage;
    @SerializedName("firstName")
    @Expose
    public String firstName;
    @SerializedName("lastName")
    @Expose
    public String lastName;
    @SerializedName("patronymic")
    @Expose
    public String patronymic;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("inn")
    @Expose
    public String inn;
    @SerializedName("status")
    @Expose
    public String status;
}