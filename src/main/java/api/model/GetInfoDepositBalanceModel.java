package api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetInfoDepositBalanceModel {

    @SerializedName("depositAmount")
    @Expose
    public Double depositAmount;
    @SerializedName("orderReserveAmount")
    @Expose
    public Double orderReserveAmount;
    @SerializedName("balanceFree")
    @Expose
    public Double balanceFree;
    @SerializedName("ordersLimitAmount")
    @Expose
    public Double ordersLimitAmount;
    @SerializedName("ordersLimitBalance")
    @Expose
    public Double ordersLimitBalance;
    @SerializedName("paymentAwaitingContractors")
    @Expose
    public Double paymentAwaitingContractors;
    @SerializedName("paymentAwaitingClient")
    @Expose
    public Double paymentAwaitingClient;
    @SerializedName("errorCode")
    @Expose
    public String errorCode;
    @SerializedName("errorMessage")
    @Expose
    public String errorMessage;
}