package api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthLoginModel {

    @SerializedName("accessToken")
    @Expose
    public String accessToken;
    @SerializedName("refreshToken")
    @Expose
    public String refreshToken;
    @SerializedName("role")
    @Expose
    public String role;
    @SerializedName("clientId")
    @Expose
    public String clientId;
    @SerializedName("clientUserId")
    @Expose
    public String clientUserId;
    @SerializedName("accessTokenExpirationDateTimeUTC")
    @Expose
    public String accessTokenExpirationDateTimeUTC;
    @SerializedName("refreshTokenExpirationDateTimeUTC")
    @Expose
    public String refreshTokenExpirationDateTimeUTC;
    @SerializedName("success")
    @Expose
    public Boolean success;
    @SerializedName("errorCode")
    @Expose
    public String errorCode;
    @SerializedName("errorMessage")
    @Expose
    public String errorMessage;

}
