package api.service;

import api.model.GetInfoDepositBalanceModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetInfoDepositBalance {
    @GET("deposit/getInfoDepositBalance/")
    Call<GetInfoDepositBalanceModel> getInfoDepositBalance(@Header("Authorization") String bearerAuthString);
}
