package api.service;

import api.model.TaxCheckContractorStatusModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface TaxCheckContractorStatus {
    @GET("tax/checkContractorStatus/")
    Call<TaxCheckContractorStatusModel> checkContractorStatus(@Header("Authorization") String bearerAuthString,
                                                              @Query("contractorInn") String contractorInn,
                                                              @Query("contractorPhone") String contractorPhone);
}
