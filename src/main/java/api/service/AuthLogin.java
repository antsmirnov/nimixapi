package api.service;

import data.Credentials;
import api.model.AuthLoginModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface AuthLogin {
    @POST("auth/login/")
    @Headers("Content-Type: application/json")
    Call<AuthLoginModel> login(@Body Credentials credentials);
}
