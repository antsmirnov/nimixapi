package testSteps;

import api.Api;
import api.model.TaxCheckContractorStatusModel;
import db.DB;
import db.Queries;
import db.entities.Contractor;
import io.qameta.allure.Step;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ContractorStatus {
    @Step
    public static void checkContractorStatusWithValidInnAndPhone() {

        Contractor validContractor = DB.postgresDB.withExtension(Queries.class, query -> {
            return query.fullRegistredContractors();
        }).get(0);

        try {
            TaxCheckContractorStatusModel response = Api.checkContractorStatus.checkContractorStatus(
                    "Bearer " + Api.savedAccessToken,
                    validContractor.inn,
                    validContractor.phone
            ).execute().body();
            assertThat(response, notNullValue());
            assertThat("Статус должен быть = 'ОК'", response.status, comparesEqualTo("OK"));
            assertThat("Поле 'phone' должно совпадать с параметром запроса 'contractorPhone'", response.phone, equalTo(validContractor.phone));
            assertThat("Поле 'inn' должно совпадать с параметром запроса 'contractorInn'", response.inn, equalTo(validContractor.inn));
        } catch (IOException e) {
            assertThat("Запрос должен выполниться успешно", e, nullValue());
        }
    }

}
