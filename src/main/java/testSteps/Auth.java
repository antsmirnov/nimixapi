package testSteps;

import data.Credentials;
import api.Api;
import api.model.AuthLoginModel;
import io.qameta.allure.Description;
import io.qameta.allure.Step;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class Auth {
    @Step
    @Description("Получение и сохранение токенов с помощью auth/login")
    public static void getAndSaveTokens() {
        try {
            AuthLoginModel loginResponse = Api.login.login(new Credentials()).execute().body();
            assertThat("Ответ содержит непустое поле 'accessToken'", loginResponse.accessToken, not(emptyOrNullString()));
            assertThat("Ответ содержит непустое поле 'refreshToken'", loginResponse.refreshToken, not(emptyOrNullString()));
            Api.savedAccessToken = loginResponse.accessToken;
            Api.savedRefreshToken = loginResponse.refreshToken;
        } catch (Exception e) {
            assertThat("Запрос должен выполниться успешно", e, nullValue());
        }
    }

    @Step
    @Description("Получение и сохранение токенов с помощью auth/refreshToken")
    public static void getAndSaveTokensWithRefreshToken() {
        try {
            AuthLoginModel loginResponse = Api.refreshToken.refreshToken("Bearer " + Api.savedRefreshToken) .execute().body();
            assertThat("Ответ содержит непустое поле 'accessToken'", loginResponse.accessToken, not(emptyOrNullString()));
            assertThat("Ответ содержит непустое поле 'refreshToken'", loginResponse.refreshToken, not(emptyOrNullString()));
            Api.savedAccessToken = loginResponse.accessToken;
            Api.savedRefreshToken = loginResponse.refreshToken;
        } catch (IOException e) {
            assertThat("Запрос должен выполниться успешно", e, nullValue());
        }
    }
}
