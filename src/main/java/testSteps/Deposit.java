package testSteps;

import api.Api;
import api.model.GetInfoDepositBalanceModel;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class Deposit {

    public static void depositBalanceIsAccessible() {
        try {
            GetInfoDepositBalanceModel response = Api.getInfoDepositBalance.getInfoDepositBalance("Bearer " + Api.savedAccessToken).execute().body();
            assertThat("Ответ содержит поле depositAmount", response.depositAmount, not(nullValue()));
        } catch (IOException e) {
            assertThat("Запрос должен выполниться успешно", e, nullValue());
        }
    }
}
