package Tests;

import org.testng.annotations.Test;

public class Auth extends BaseTest {

    @Test(description = "Проверка получения рабочего AccessToken-а")
    public void gettingGoodAccessTokens() {
        testSteps.Auth.getAndSaveTokens();
        testSteps.Deposit.depositBalanceIsAccessible();
    }

    @Test(description = "Проверка получения AccessToken-а методом refreshToken", dependsOnMethods = "gettingGoodAccessTokens")
    public void gettingGoodAccessTokensViaRefreshToken() {
        testSteps.Auth.getAndSaveTokensWithRefreshToken();
        testSteps.Deposit.depositBalanceIsAccessible();
    }
}
