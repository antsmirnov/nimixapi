package Tests;

import jdk.jfr.Description;
import org.testng.annotations.Test;

public class ContractorStatus extends BaseTest {

    @Test
    @Description("checkContractorStatus - проверка ответа при отправке существующего телефона и ИНН")
    public void checkContractorStatusWithValidInnAndPhone() {
        testSteps.ContractorStatus.checkContractorStatusWithValidInnAndPhone();
    }
}
