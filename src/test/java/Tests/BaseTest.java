package Tests;

import org.testng.annotations.BeforeSuite;

public abstract class BaseTest {

    @BeforeSuite
    public void setup() {
        testSteps.Auth.getAndSaveTokens();
    }
}
